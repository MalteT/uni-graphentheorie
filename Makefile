FILES := \
	graphentheorie.tex \
	setup/declarations.tex \
	setup/metadata.tex \
	setup/packages.tex \
	lectures/01.tex

all: $(FILES)
	latexmk graphentheorie.tex -pdf

clean:
	latexmk -c $(FILES) && git clean -dfX
