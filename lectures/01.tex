\section{Grundlagen}

\begin{definition}[Graph]
    Ein Graph $G = (V, E)$ ist ein Tupel aus den 2 Mengen
    \begin{itemize}
        \item Knotenmenge $x, y, z \in V$
        \item Kantenmenge $e, f, g \in E \subset V^2$ mit $V^2 = \left\{ \{x, y\} \mid x, y \in V, x \not = y \right\}$.\\
            In \emph{ungerichteten} Graphen gilt für eine Kante $f =\{x, y\} \equiv \{y, x\}$.\\
            ($\equiv$ ist die Equivalenzrelation)
    \end{itemize}

    \begin{figure}[htb]
        \centering
        \begin{tikzpicture}
            \node[draw, fill=black, circle, inner sep=0pt, minimum size=5pt, label=$x$](x) at (3, 3.5){};
            \node[draw, fill=black, circle, inner sep=0pt, minimum size=5pt, label=$y$](y) at (1.4, 2){};
            \node[draw, fill=black, circle, inner sep=0pt, minimum size=5pt, label=$z$](z) at (0, 4){};

            \draw [thick] (x) -- (y) node [midway, below] {$f$};
            \draw [thick] (y) -- (z) node [midway, below] {$g$};
            \draw [thick] (z) -- (x) node [midway, below] {$e$};
        \end{tikzpicture}
        \caption{Darstellung eines Graph}
    \end{figure}
\end{definition}

\subsection{Einige benannte Graphentypen}
\begin{description}
    \item[einfacher Graph] In einem einfachen Graphen sind zwei Knoten nur über jeweils eine Kante direkt verbunden.\\
        \begin{tikzpicture}
            \node[draw, fill=black, circle, label=$x$, inner sep=0pt, minimum size=5pt](x) at (0, 0){};
            \node[draw, fill=black, circle, label=$y$, inner sep=0pt, minimum size=5pt](y) at (1, 0){};
            \draw [thick] (x) -- (y);
        \end{tikzpicture}

    \item[Multigraph] Zwei Knoten können in einem Multigraph über mehrere Kanten direkt verbunden sein.\\
        \begin{tikzpicture}
            \node[draw, fill=black, circle, label=$x$, inner sep=0pt, minimum size=5pt](x) at (0, 0){};
            \node[draw, fill=black, circle, label=$y$, inner sep=0pt, minimum size=5pt](y) at (1, 0){};
            \draw [thick] (x) .. controls (0.5, -0.5) .. (y);
            \draw [thick] (x) .. controls (0.5, 0.5) .. (y);
            \draw [thick] (x) -- (y);
        \end{tikzpicture}
    \item[Schleifenfrei] (oder \emph{loop-free}) sind Graphen, in denen ein Knoten $x$ nicht direkt mit sich selbst verbunden ist ($\forall x \in V: \{x, x\} \not\in E$).
        Eine Schleife:\\
        \begin{tikzpicture}
            \node[draw, fill=black, circle, label=$x$, inner sep=0pt, minimum size=5pt](x) at (0, 0){};
            \draw [thick] (x) .. controls (1, 0.5) and (1, -0.5) .. (x);
        \end{tikzpicture}
    \item[gerichteter Graph] Der Kante wird eine Ordnung gegeben. Dadurch wird aus der Menge $\{x, y\}$ die eine Kante beschreibt, ein Tupel $(x, y)$. $x$ wird in dem Fall als \emph{Tail} (Schwanz) und $y$ als \emph{Head} (Kopf) bezeichnet.
        Die Relation geht dabei von Head in Richtung Tail aus.
        Die für ungerichtete Graphen beschriebene Equivalenzrelation gilt nicht mehr ($(x, y) \not\equiv (y, x)$).
        Im Bild wird eine gerichtete Kante als Pfeil dargestellt: 
        \begin{tikzpicture}
            \node[draw, fill=black, circle, label=$x$, inner sep=0pt, minimum size=5pt](x) at (0, 0){};
            \node[draw, fill=black, circle, label=$y$, inner sep=0pt, minimum size=5pt](y) at (1, 0){};
            \draw [->, >=stealth, thick] (x) -- (y);
        \end{tikzpicture}
\end{description}

\newpage
\subsection{Definitionen}
\begin{definition}[Kantenzug]
    Ein Kantenzug ist eine Sequenz von über Kanten zusammenhängende Knoten: $(x_1, e_1, x_2, e_2, x_3, e_3, x_4)$. Zwei aufeinanderfolgende Kanten enthalten immer mindestens einen gemeinsamen Knoten $(\dots e', x, e" \dots)\quad x \in e' \cap e"$.

    \begin{figure}[!htb]
        \centering
        \begin{tikzpicture}
            \node [draw, fill=black, circle, inner sep=0pt, minimum size=5pt, label=$x_1$](x1) at (-3, 0.75) {};
            \node [draw, fill=black, circle, inner sep=0pt, minimum size=5pt] (xl) at (-3, -0.75) {};
            \node [draw, fill=black, circle, inner sep=0pt, minimum size=5pt, label=$x_2$](x2) at (-1, 0) {};

            \node [draw, fill=black, circle, inner sep=0pt, minimum size=5pt] (xr) at (3, 0.75) {};
            \node [draw, fill=black, circle, inner sep=0pt, minimum size=5pt, label=$x_4$] (x4) at (3, -0.75) {};
            \node [draw, fill=black, circle, inner sep=0pt, minimum size=5pt, label=$x_3$] (x3) at (1, 0) {};

            \draw [very thick] (x1) -- (x2) node [midway, below] {$e_1$};
            \draw [very thick] (x2) -- (x3) node [midway, below] {$e_2$};
            \draw [very thick] (x3) -- (x4) node [midway, below] {$e_3$};

            \draw [very thin] (x1) -- (xl);
            \draw [very thin] (x2) -- (xl);
            \draw [very thin] (x3) -- (xr);
            \draw [very thin] (x4) -- (xr);

        \end{tikzpicture}
        \caption{Darstellung eines Kantenzugs}
    \end{figure}
\end{definition}

\begin{definition}[Zusammenhängender Graph]
    Ein Graph $G = (V, E)$ ist \emph{zusammenhängend}, wenn je zwei Knoten $x, y$ durch einen Kantenzug verbunden sind. 
\end{definition}

\begin{definition}[Weg]
    Ein \emph{Weg} ist ein Kantenzug in dem jede Kante maximal einmal vorkommt.
\end{definition}

\begin{definition}[Pfad]
    Ein \emph{Pfad} ist ein Kantenzug in dem jeder Knoten (ausgenommen erster $=$ letzter) nur einmal vorkommt.
\end{definition}

\begin{definition}[Länge]\marginnote{\textbf{Anmerkung:}\\$|M|$ ist die Kardinalität einer Menge $M$ -- die Anzahl ihrer Elemente.}
    Die \emph{Länge} eines Kantenzugs ist die Anzahl der Kanten. Ein einzelner Knoten $(x_1)$ ist ein Kantenzug der Länge $0$.\\
    $\rightarrow$ Die Länge von Wegen ist durch $|E|$ beschränkt.
\end{definition}

\begin{definition}[Offene und Geschlossene Kantenzüge]
    Pfade, Wege und Kantenzüge sind \emph{offen}, wenn $x_1 \not = x_n$ ($x_n$ ist der letzte Knoten im Kantenzug) und geschlossen, wenn $x_1 = x_n$.
\end{definition}

\begin{definition}[Kreis]\label{def:kreis}\hfill
    \begin{itemize}[noitemsep]
        \item (elementarer) Kreis: geschlossener Pfad, jeder Knoten im Kreis hat genau 2 Nachbarn
        \item verallgemeinerter Kreis: geschlossener Weg
    \end{itemize}
\end{definition}

\begin{definition}[Knotengrad]
    Der \emph{Knotengrad} ($\deg(x)$) ist die Anzahl der Nachbarn eines Knotens = $\left| \{y \in V \mid \{x, y\} \in E\} \right| = $ Anzahl inzidenter Kanten.
    $e$ ist inzident zu $x \Leftrightarrow x \in e$.

    Für gerichtete Graphen:
    \begin{align*}
        \deg_\text{in}(x) &= |\{y \mid (y, x) \in E\}| \qquad\tikz[baseline=-0.5ex]{
            \node [draw, circle, inner sep=0pt, minimum size=3pt](x) at (0,0){};
            \node (a) at (-0.4, 0.4){};
            \node (b) at (0.4, 0.4){};
            \node (c) at (0.4, -0.4){};
            \node (d) at (-0.4, -0.4){};
            \draw [->, >=stealth] (a) -- (x);
            \draw [->, >=stealth] (b) -- (x);
            \draw [->, >=stealth] (c) -- (x);
            \draw [->, >=stealth] (d) -- (x);
        }\\
        \deg_\text{out}(x) &= |\{y \mid (x, y) \in E\}|\qquad\tikz[baseline=-0.5ex]{
            \node [draw, circle, inner sep=0pt, minimum size=3pt](x) at (0,0){};
            \node (a) at (-0.4, 0.4){};
            \node (b) at (0.4, 0.4){};
            \node (c) at (0.4, -0.4){};
            \node (d) at (-0.4, -0.4){};
            \draw [->, >=stealth] (x) -- (a);
            \draw [->, >=stealth] (x) -- (b);
            \draw [->, >=stealth] (x) -- (c);
            \draw [->, >=stealth] (x) -- (d);
        }
        \\
    \end{align*}
\end{definition}

\begin{lemma}\hfill\newline
    Jeder Kantenzug von $x$ nach $p$ enthält einen Weg von $x$ nach $p$.\\
    Jeder Kantenzug von $x$ nach $p$ enthält einen Pfad von $x$ nach $p$.
\end{lemma}

\begin{definition}[Abstand]
    Wenn $G$ zusammenhängend ist, dann sei der \emph{Abstand} $\dist(x, y)$ von zwei Knoten $x, y$ die Länge des \emph{kürzesten} Pfades zwischen $x$ und $y$.
    \begin{align*}
        \dist(x, x) &= 0 & \text{(Identität)}\\
        \dist(x, y) &= \dist(y, x) &\text{(Symmetrie)}\\
        \dist(x, y) &\leq \dist(x, z) + \dist(z, y) & \text{(Dreiecksungleichung)}
    \end{align*}
    $\rightarrow$ somit ist $d$ eine Metrik.
\end{definition}

\begin{definition}[Eulerscher Kreis]
    Ein geschlossener Weg der genau alle Kanten enthält heißt \emph{Eulerscher Kreis}.
    Enthält ein Graph einen Eulerschen Kreis, nennt man ihn \emph{Eulerschen Graph}.\\
    Notwendige Bedingungen für Eulersche Graphen:
    \begin{itemize}[noitemsep]
        \item $G$ ist zusammenhängend
        \item Jeder Knoten in $G$ hat geraden Grad
    \end{itemize}
\end{definition}


\subsubsection*{Zerlegung von Eulergraphen}
\marginnote{TODO: Zerlegung ausführlicher}

Ein ``innerer'' Kreis in $E$ ist $C$ (Abbildung \vref{img:euler}).

\begin{figure}[!htb]
    \centering
    \begin{tikzpicture}
		\node [draw, circle, fill=black, inner sep=0pt, minimum size=5pt] (0) at (0, 0.5) {};
		\node [draw, circle, fill=black, inner sep=0pt, minimum size=5pt] (1) at (1, 0) {};
		\node [draw, circle, fill=black, inner sep=0pt, minimum size=5pt] (2) at (3, -1) {};
		\node [draw, circle, fill=black, inner sep=0pt, minimum size=5pt] (3) at (1.5, -1) {};


        \draw [thick] (0) .. controls ++(90:-0.5) and ++(180:0.5) .. (1);
        \draw [thick] (1) .. controls ++(180:-2) and ++(90:0.5) .. (2);
        \draw [thick, name path=A] (2) .. controls ++(90:-0.5) and ++(270:0.5) .. (3);
        \draw [thick, name path=B] (3) .. controls ++(270:-0.5) and ++(90:0.5) .. (2);
        \draw [thick] (2) .. controls ++(90:-1) and ++(270:3) .. (1);
        \draw [thick] (1) .. controls ++(270:-1) and ++(90:0.5) .. (0);

        \tikzfillbetween[of=A and B]{pattern=north west lines, pattern color=gray};
        \node [circle, fill=white, inner sep=0pt] at(2.25, -1) {$C$};

    \end{tikzpicture}
    \caption{\label{img:euler}Eulerscher Graph}
\end{figure}

In $G\setminus C$\marginnote{\textbf{Anmerkung:}\\ $G\setminus C$: Menge $G$ ohne Menge $C$} hat jeder Knoten wieder geraden Grad:
\begin{align*}
    \deg_{G\setminus C}(x) & = \deg_G(x) & \text{wenn } x \not \in C \\ 
    \deg_{G\setminus C}(x) & = \deg_G(x) -2 & \text{wenn } x \text{ auf } C \text{ liegt} \\
\end{align*}
Jeder Eulerkreis kann in eine Menge von \emph{Kanten-disjunkte}, elementare Kreisen zerlegt werden.

\begin{definition}[Adjazenz-Matrix]
Eine Adjazenz-Matrix $A$ ist eine Möglichkeit, einen Graphen numerisch zu beschreiben.
Die Elemente werden dabei folgendermaßen berechnet:
\begin{equation*}
A_{xy} = 
\begin{cases}
    1 & \mbox{wenn } \{x, y\} \in E \\
    0 & \mbox{sonst}
\end{cases}
\end{equation*}
Die Spalten- und Zeilen-Indizes sind also die Knoten-Indizes.
Ist ein Knoten mit einem anderen verbunden, ist das korrespondierende Element in der Matrix $1$, ansonsten $0$.
Die Elemente der Hauptdiagonalen der Adjazenz-Matrix sind $0$ für einen schleifenfreien Graph.
Ist der Graph ungerichtet, so ist die Matrix symmetrisch (spiegelsymmetrisch bezüglich der Hauptdiagonalen).
Ein Beispiel:
\begin{align*}
    G:& \tikz[baseline=0pt]{
        \node [draw, circle, fill=black, inner sep=0pt, minimum size=3pt, label=$1$] (1) at (-1, 0.5) {};
        \node [draw, circle, fill=black, inner sep=0pt, minimum size=3pt, label=$2$] (2) at (0, 0) {};
        \node [draw, circle, fill=black, inner sep=0pt, minimum size=3pt, label=below:$3$] (3) at (-1, -0.5) {};
        \node [draw, circle, fill=black, inner sep=0pt, minimum size=3pt, label=$4$] (4) at (1, 0) {};
        \draw [] (2) -- (3) -- (1) -- (2) -- (4);
    }
    & A =
    \begin{pmatrix}
        0 & 1 & 1 & 0 \\
        1 & 0 & 1 & 1 \\
        1 & 1 & 0 & 0 \\
        0 & 1 & 0 & 0 \\
    \end{pmatrix}\\
    G':& \tikz[baseline=0pt]{
        \node [draw, circle, fill=black, inner sep=0pt, minimum size=3pt, label=$3$] (1) at (-1, 0.5) {};
        \node [draw, circle, fill=black, inner sep=0pt, minimum size=3pt, label=$2$] (2) at (0, 0) {};
        \node [draw, circle, fill=black, inner sep=0pt, minimum size=3pt, label=below:$4$] (3) at (-1, -0.5) {};
        \node [draw, circle, fill=black, inner sep=0pt, minimum size=3pt, label=$1$] (4) at (1, 0) {};
        \draw [] (2) -- (3) -- (1) -- (2) -- (4);
    } & A' =
    \begin{pmatrix}
        0 & 1 & 0 & 0 \\
        1 & 0 & 1 & 1 \\
        0 & 1 & 0 & 1 \\
        0 & 1 & 1 & 0 \\
    \end{pmatrix}
\end{align*}
Problem: Die Graphen $G$ und $G'$ beschreiben eigentlich den gleichen Graphen, jedoch haben sie verschiedene Adjazenz-Matrizen!
\end{definition}


\begin{definition}[Graph Isomorphie]
    Ein Isomorphismus von $G \to G'$ ist eine bijektive (also umkehrbare) Abbildung $\varphi: V(G) \to V(G')$.
    Für die Kanten:
    \begin{equation*}
        \{x, y\} \in E(G) \Leftrightarrow \{\varphi(x), \varphi(y)\} \in E(G')
    \end{equation*}
\end{definition}

Generell ist das Finden von Isomorphien sehr schwer.
Einige notwendige Bedingungen:
\begin{itemize}
    \item $|V_1| = |V_2|$ -- Kardinalitäten der Knotenmengen sind gleich
    \item $|E_1| = |E_2|$ -- Kardinalitäten der Kantenmengen sind gleich
    \item Gradsequenzen der Graphen sind gleich\\
        ($\forall x \in V: \deg_G(x) = \deg_{G'}(\varphi(x))$)\\
        Eine Gradsequenz ist dabei die Sequenz der Anzahl von Knoten pro Knotengrad, beginnend bei $0$.
        Ein Knotengrad von $0$ bedeutet, dass der Knoten keinen Nachbarn hat.
        Besteht ein Graph also zum Beispiel aus $2$ isolierten Knoten, so ist seine Gradsequenz $2$.
        Ein Graph mit einem isolierten Knoten und $2$ miteinander verbundenen Knoten hat eine Gradsequenz von $1 2$.
    \item Andere Graph-Invarianten
\end{itemize}

\begin{figure}[!htb]
    \centering
    \begin{minipage}{.3\textwidth}
        \centering
        \begin{tikzpicture}[scale=0.5]
            \node [draw, circle, fill=black, inner sep=0pt, minimum size=3pt] (1) at (-1, 0.5) {};
            \node [draw, circle, fill=black, inner sep=0pt, minimum size=3pt] (2) at (0, 0) {};
            \node [draw, circle, fill=black, inner sep=0pt, minimum size=3pt] (3) at (-1, -0.5) {};
            \node [draw, circle, fill=black, inner sep=0pt, minimum size=3pt] (4) at (1, 0) {};
            \draw [] (2) -- (3) -- (1) -- (2) -- (4);
            \node [] at (0, -1) {0 1 2 1};
        \end{tikzpicture}
    \end{minipage}
    \begin{minipage}{.3\textwidth}
        \centering
        \begin{tikzpicture}[scale=0.5]
            \node [draw, circle, fill=black, inner sep=0pt, minimum size=3pt] (1) at (-1, 0.5) {};
            \node [draw, circle, fill=black, inner sep=0pt, minimum size=3pt] (2) at (0, 0) {};
            \node [draw, circle, fill=black, inner sep=0pt, minimum size=3pt] (3) at (-1, -0.5) {};
            \node [draw, circle, fill=black, inner sep=0pt, minimum size=3pt] (4) at (1, 0) {};
            \node [draw, circle, fill=black, inner sep=0pt, minimum size=3pt] (5) at (-.75, 0) {};
            \draw [] (2) -- (3) -- (1) -- (2) -- (4);
            \node [] at (0, -1) {1 1 2 1};
        \end{tikzpicture}
    \end{minipage}
    \begin{minipage}{.3\textwidth}
        \centering
        \begin{tikzpicture}[scale=0.5]
            \node [draw, circle, fill=black, inner sep=0pt, minimum size=3pt] (1) at (-1, 0.5) {};
            \node [draw, circle, fill=black, inner sep=0pt, minimum size=3pt] (2) at (0, 0) {};
            \node [draw, circle, fill=black, inner sep=0pt, minimum size=3pt] (3) at (-1, -0.5) {};
            \node [draw, circle, fill=black, inner sep=0pt, minimum size=3pt] (4) at (1, 0) {};
            \node [draw, circle, fill=black, inner sep=0pt, minimum size=3pt] (5) at (.5, .25) {};
            \node [draw, circle, fill=black, inner sep=0pt, minimum size=3pt] (5) at (.5, -.25) {};
            \draw [] (2) -- (3) -- (1) -- (2) -- (4);
            \node [] at (0, -1) {2 1 2 1};
        \end{tikzpicture}
    \end{minipage}
    \caption{Gradsequenzen dreier Graphen}
\end{figure}

\begin{definition}[Graph-Invarianten]
    $f: \text{Graph} \to X$ heißt Graph-Invariante wenn gilt:
    $G$ isomorph zu $H$ ($H \simeq G$), dann gilt $f(G) = f(H)$.
\end{definition}

Graph-Isomorphie kann auch über Permutation der Adjazenz-Matrizen gefunden werden: $A' = P\cdot A \cdot P^{-1}$ ($P$ ist eine \href{https://de.wikipedia.org/wiki/Permutationsmatrix}{Permutationsmatrix})\\
$\rightarrow$ $A$ und $A'$ haben die selben Eigenwerte\\
$\rightarrow$ Eigenwerte der Adjazenz-Matrizen sind eine Graph-Invariante!

\begin{definition}[Gradmatrix]\label{def:gradmatrix}
    Die Gradmatrix ist eine Diagonalmatrix der Knotengrade.
    Sie kann aus der Adjazenz-Matrix durch $\forall x\in V: \deg(x) = \sum_{y\in V}A_{xy}$ berechnet werden.
    Also:
    \begin{equation*}
        \text{Gradmatrix} = \begin{pmatrix}
            \deg(x_1) &         0 & \dots    & 0\\
            0 & \deg(x_2) &         & 0\\
            \vdots &           & \ddots  & 0\\
            0   &     0  &     0 & \deg(x_n)\\
        \end{pmatrix}
    \end{equation*}
\end{definition}
Beispiel:
\begin{align*}
    G:& \tikz[baseline=0pt]{
        \node [draw, circle, fill=black, inner sep=0pt, minimum size=3pt, label=$1$] (1) at (-1, 0.5) {};
        \node [draw, circle, fill=black, inner sep=0pt, minimum size=3pt, label=$2$] (2) at (0, 0) {};
        \node [draw, circle, fill=black, inner sep=0pt, minimum size=3pt, label=below:$3$] (3) at (-1, -0.5) {};
        \node [draw, circle, fill=black, inner sep=0pt, minimum size=3pt, label=$4$] (4) at (1, 0) {};
        \draw [] (2) -- (3) -- (1) -- (2) -- (4);
    }
    & D =
    \begin{pmatrix}
        2 & 0 & 0 & 0 \\
        0 & 3 & 0 & 0 \\
        0 & 0 & 2 & 0 \\
        0 & 0 & 0 & 1 \\
    \end{pmatrix}\\
\end{align*}

\begin{definition}[Laplace-Matrix]
    $L = D - A$
\end{definition}

\begin{definition}[Inzidenz-Matrix]
    \begin{equation*}
    \widetilde{H}_{xe} = 
    \begin{cases}
        1 & \mbox{wenn } x \in e \\
        0 & \mbox{sonst}
    \end{cases}
    \end{equation*}
    Für jede Kantenorientierung $e = \{x, y\}: \tail(e) = x \text{ und } \head(e) = y$.
    \begin{equation*}
    H_{xe} = 
    \begin{cases}
        +1 & \mbox{wenn } x = \head(e) \\
        -1 & \mbox{wenn } x = \tail(e) \\
        0 & \mbox{sonst}
    \end{cases}
    \end{equation*}
\end{definition}

\begin{lemma}
    $L = H \cdot H^T$
\end{lemma}

\begin{proof}\hfill\\
Gegeben:
\begin{equation*}
 L = D - A
\end{equation*}
Zu zeigen ist $\forall x, y \in V$:
    \begin{equation*}\marginnote{\textbf{Anmerkung:}\\ $H^T_{ey} = H_{ye}$}
    L_{xy} = \sum_{e} H_{xe}H^T_{ey} = \sum_{e}H_{xe}H_{ye} = \deg(x)\cdot \delta_{xy} - A_{xy}\end{equation*}
    mit\marginnote{\textbf{Anmerkung:}\\ $\deg(x)\cdot\delta_{xy}$ ist eine Konstruktion der Gradmatrix $D$ (\ref{def:gradmatrix})}
    \begin{equation*}
    \delta_{xy} = \begin{cases}
        1 & \mbox{wenn } x = y \\
        0 & \mbox{sonst}
    \end{cases}
\end{equation*}

    \noindent 2 Fälle müssen unterschieden werden: $x = y$ (Hauptdiagonale) und $x \not = y$ (unter bzw. obere Dreiecksmatrix).

    \begin{description}
        \item[1. Fall $x = y$]\hfill
            \begin{equation*}
                \sum_eH_{xe}H_{xe} = \underbrace{\sum_e\underbrace{H^2_{xe}}_{\mathrlap{= 1 \text{ wenn } x \text{ und } e \text{ inzident}}}}_{\mathrlap{\text{Anzahl Kanten, die an }x\text{ hingehen}}} = \deg (x)
            \end{equation*}
        \item[2. Fall $x \not = y$]\hfill
            \begin{equation*}
                \sum_eH_{xe}H_{ye} = -A_{xy}
            \end{equation*}
            \begin{enumerate}
                \item[a)] $x$ und $y$ sind benachbart: $\exists$ genau eine Kante $\tilde{e}$ mit $\head(\tilde{e}) = x$ und $\tail(\tilde{e}) = y$ oder eine Kante $\hat{e}$ mit $\head(\hat{e}) = y$ und $\tail(\hat{e}) = x$
                    \begin{align*}
                        \tilde{e}\in E(G)&:& 1 \cdot (-1) &=& -1 &=& A_{xy} \\
                        \hat{e}\in E(G)&:&\quad (-1)\cdot 1&=& -1 &=& A_{xy}
                    \end{align*}
                \item[b)] $x$ und $y$ sind nicht benachbart $\Rightarrow$ Für jede Kante gilt $x \not\in e$ oder $y \not\in e \Rightarrow H_{xe}H_{ye} = 0$
            \end{enumerate}
    \end{description}

\end{proof}
