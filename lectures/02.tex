\section{Kreise}


Wiederholung Kreis (vgl. \vref{def:kreis}):
\begin{itemize}[noitemsep]
    \item Elementarer Kreis ist ein geschlossener Pfad.
        Jeder Knoten im Kreis hat einen Grad von 2.
    \item Verallgemeinerter Kreis ist eine disjunkte Vereinigung von geschlossenen Wegen.
        \emph{Der Knotengrad ist gerade.}
\end{itemize}

\begin{figure}[!htb]
    \centering
    \begin{minipage}{.4\textwidth}
        \centering
        \begin{tikzpicture}[scale=0.5]
            \coordinate (1) at (0, 0);
            \coordinate (2) at (-1, -1);
            \coordinate (3) at (0, -2);
            \coordinate (4) at (-1, -3);
            \coordinate (5) at (1, -3);
            \coordinate (6) at (1.5, -3);
            \coordinate (7) at (1.5, -2);
            \coordinate (8) at (1, -1);

            \draw [-] (1) -- (2) -- (3) -- (4) -- (5) -- (6) -- (7) -- (5) -- (3) -- (8) -- (1);
        \end{tikzpicture}
    \end{minipage}
    \begin{minipage}{.4\textwidth}
        \centering
        \begin{tikzpicture}[scale=0.5]
            \coordinate (11) at (-2, 0);
            \coordinate (12) at (-3, -1);
            \coordinate (13) at (-2, -2);
            \coordinate (14) at (-1, -1);

            \draw [-] (11) -- (12) -- (13) -- (14) -- (11);

            \coordinate (21) at (2, 0);
            \coordinate (22) at (3, -1);
            \coordinate (23) at (2, -2);
            \coordinate (24) at (1, -1);

            \draw [-] (21) -- (22) -- (23) -- (24) -- (21);

            \coordinate (1) at (-3, -2.5);
            \coordinate (2) at ( 3, -2.5);
            \draw [decorate,decoration={brace,amplitude=5pt}] (2) -- (1) node[midway, below, yshift=-5pt]{\scriptsize disjunkt};
        \end{tikzpicture}
    \end{minipage}
\end{figure}

\subsection{Kantenoperationen}
Wir identifizieren (verallg.) Kreise mit ihren Kantenmengen.
Einen Graphen ohne isolierte Knoten können wir generell allein durch die Kantenmenge beschreiben, da die Knoten in den Kanten enthalten sind.
Die leere Kantenmenge $\varnothing$ sei auch ein Kreis (Knotengrad ist $0$ und daher gerade).

\begin{definition}[Charakteristischer Kantenvektor]
Wir definieren den \emph{charakteristischen Kantenvektor} folgendermaßen:
\begin{equation*}
    F \subseteq E(G)\qquad F =
    \begin{pmatrix}
        f_1\\f_2\\\vdots\\f_n
    \end{pmatrix}
    \qquad
    f_e =
    \begin{cases}
        1 & \mbox{wenn } e\in F\\
        0 & \mbox{wenn } e\not\in F
    \end{cases}
\end{equation*}
\end{definition}
Beispiel:
\begin{equation*}
    \tikz[scale=1.2, baseline=0]{
        \coordinate (1) at (0, 0.5);
        \coordinate (2) at (0.6, .2);
        \coordinate (3) at (1, 0.35);
        \coordinate (4) at (0.3, -.1);
        \coordinate (5) at (0, -0.5);
        \coordinate (6) at (0.7, -0.5);

        \draw[-] (1) -- (2) node[midway, above]{\scriptsize $5$} -- (4) node[midway, left, yshift=2pt]{\scriptsize $4$} -- (5) node[midway, above]{\scriptsize $1$} -- (6)node[midway, below]{\scriptsize $2$} -- (4)node[midway, right, yshift=2pt]{\scriptsize $3$};
        \draw[-] (2) -- (3)node[midway, above]{\scriptsize 6};
        \draw[-, very thick] (2) -- (4) -- (5);
        \draw[-, very thick] (4) -- (6);
    }
    \rightarrow F =
    \begin{pmatrix}
        1\\0\\1\\1\\0\\0
    \end{pmatrix}
    \qquad
    \sum H_{xe}\cdot F_e = \sum_{e\in F}H_{xe} = \deg_F(x)
\end{equation*}

\begin{definition}[Kantensumme (XOR)]\marginnote{\textbf{Anmerkung:}\\Der Prof. hat hier EXOR an die Tafel geschrieben, was aber eine sehr unübliche Bezeichnung für die Antivalenz (exklusives Oder) ist.}
    Eine Kante ist in der Summe zweier Graphen enthalten, wenn sie in nur einem der Summanden enthalten ist.
    Die folgende Wahrheitswertetabelle ($G_1$ vs $G_2$) soll veranschaulichen:

    \begin{table}[!htb]
        \centering
        \begin{tabular}{c|cc}
            $\oplus$ & $0$ & $1$\\
            \midrule
            $0$ & $0$ & $1$ \\
            $1$ & $1$ & $0$
        \end{tabular}
    \end{table}
\end{definition}

\pagebreak
\begin{definition}[Kantenprodukt (AND)]
    Das Kantenprodukt ist ähnlich zur Kantensumme über eine logische Operation definiert.
    Hier jedoch, statt der Antivalenz (XOR), die Konjunktion (logisches Und):

    \begin{table}[!htb]
        \centering
        \begin{tabular}{c|cc}
            $\bullet$ & $0$ & $1$\\
            \midrule
            $0$ & $0$ & $0$ \\
            $1$ & $0$ & $1$
        \end{tabular}
    \end{table}
\end{definition}

Folgendes gilt nur, wenn $F$ ein verallgemeinerter Kreis ist, also $\deg_F(x)$ gerade ist:
\begin{equation*}
    \bigoplus_{e\in F} H_{xe}F_e = 0 \qquad \forall x \in V
\end{equation*}
also
\begin{equation*}
    H\cdot F = \vec{o} \Leftrightarrow F \mbox{ ist verallgemeinerter Kreis}
\end{equation*}

Damit bildet $F$ einen Vektorraum: $\left\{ F \mid H\cdot F = \vec{o} \right\}$.
Dieser Vektorraum ist abgeschlossen unter $\oplus$, wenn wir also zwei verallgemeinerter Kreise $A$ und $B$ haben, so ist $A \oplus B$ wieder ein verallgemeinerter Kreis.

\noindent Beispiel:
\begin{figure}[!htb]
    \centering
    \begin{tikzpicture}
        \node[draw, circle, fill=black, inner sep=0pt, minimum size=2pt] (1) at (0, 0) {};
        \node[draw, circle, fill=black, inner sep=0pt, minimum size=2pt] (2) at (1, 0) {};
        \node[draw, circle, fill=black, inner sep=0pt, minimum size=2pt] (3) at (1.5, -0.5) {};
        \node[draw, circle, fill=black, inner sep=0pt, minimum size=2pt] (4) at (2, 0) {};
        \node[draw, circle, fill=black, inner sep=0pt, minimum size=2pt] (5) at (2.5, 0.5) {};
        \node[draw, circle, fill=black, inner sep=0pt, minimum size=2pt] (6) at (2, 1) {};
        \node[draw, circle, fill=black, inner sep=0pt, minimum size=2pt] (7) at (1, 1) {};
        \node[draw, circle, fill=black, inner sep=0pt, minimum size=2pt] (8) at (0.5, 1.5) {};
        \node[draw, circle, fill=black, inner sep=0pt, minimum size=2pt] (9) at (0, 1) {};

        \draw[-] (1) -- (2) -- (3) -- (4) -- (5) -- (6) -- (7) -- (8) -- (9) -- (1);
        \draw[-] (9) -- (7) -- (2) -- (4) -- (6);

        \draw[-, very thick, RoyalBlue]
            ($(1)+(-0.1, -0.1)$)
            -- ($(2)+(-0, -0.1)$)
            -- ($(4)+(0.1, -0.1)$)
            -- ($(6)+(0.1, 0.1)$)
            -- ($(7)+(0, 0.1)$)
            -- ($(8)+(0, 0.14)$)
            -- ($(9)+(-0.1, 0)$)
            -- ($(1)+(-0.1, -0.1)$);

        \draw[-, very thick, RedOrange]
            ($(2)+(0.1, 0)$)
            -- ($(3)+(0, 0.1)$)
            -- ($(4)+(-0.1, 0)$)
            -- ($(5)+(-0.1, 0)$)
            -- ($(6)+(0, -0.1)$)
            -- ($(7)+(0.1, -0.1)$)
            -- ($(2)+(0.1, 0)$);
    \end{tikzpicture}
\end{figure}
